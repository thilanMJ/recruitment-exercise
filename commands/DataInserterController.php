<?php

namespace app\commands;

use app\models\Loan;
use app\models\User;
use yii\console\Controller;
use yii\helpers\FileHelper;
use yii\helpers\Json;

class DataInserterController extends Controller
{
    public $dataSet;

    public function options($actionID)
    {
        return ['dataSet'];
    }

    public function optionAliases()
    {
        return ['d' => 'dataSet'];
    }

    public function actionIndex()
    {
        $fileData = $this->getFile();
        foreach ($fileData as $file) {
            $filePath = explode('/', $file);
            if ($filePath[(count($filePath) - 1)] == 'users.json') {
                $string = file_get_contents($file);
                $userList = json_decode($string, true);
                $command = \Yii::$app->db->createCommand();
                $command->batchInsert('user', [
                    'id',
                    'first_name',
                    'last_name',
                    'email',
                    'personal_code',
                    'phone',
                    'active',
                    'dead',
                    'lang'
                ], $userList)->execute();
            } elseif ($filePath[(count($filePath) - 1)] == 'loans.json') {
                $string = file_get_contents($file);
                $loanList = json_decode($string, true);
                foreach ($loanList as $loan) {
                    $loanModal = new Loan();
                    $loanModal->id = $loan['id'];
                    $loanModal->user_id = $loan['user_id'];
                    $loanModal->amount = $loan['amount'];
                    $loanModal->interest = $loan['interest'];
                    $loanModal->duration = $loan['duration'];
                    $loanModal->start_date = \Yii::$app->formatter->asDate($loan['start_date']);
                    $loanModal->end_date = \Yii::$app->formatter->asDate($loan['end_date']);
                    $loanModal->campaign = $loan['campaign'];
                    $loanModal->status = $loan['status'];
                    $loanModal->save();
                }
            }
        }
    }

    protected function insertData()
    {

    }

    protected function getFile()
    {
        $files = FileHelper::findFiles(\Yii::$app->getBasePath() . "/", [
            'only' => ['*.json'],
            'recursive' => false,
        ]);
        return $files;
    }
}
