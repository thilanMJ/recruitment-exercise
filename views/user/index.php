<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create User', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            //['class' => 'yii\grid\SerialColumn'],

            [// this line is optional
                'attribute' => 'id',
                'format' => 'text',
                'label' => 'ID',
                'value' => function ($model) {
                    return $model->getAttribute('id');
                }
            ],
            'first_name:ntext',
            'last_name:ntext',
            'email:ntext',
            'personal_code',
            'age',
            //'phone',
            //'active:boolean',
            //'dead:boolean',
            //'lang:ntext',
            [
                'class' => 'yii\grid\ActionColumn',
                'header' => 'Action',
                'template' => '{view} {edit} {delete}',
                'buttons' => [
                    'view' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', $url);
                    },
                    'edit' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url);
                    },
                    'delete' => function ($url, $model) {
                        return Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, [
                            'title' => Yii::t('yii', 'Delete'),
                            'data-confirm' => Yii::t('yii', 'Are you sure you want to delete this item?'),
                            'data-method' => 'post',
                        ]);
                    },
                ],
                'urlCreator' => function ($action, $model, $key, $index) {
                    if ($action === 'view') {
                        $url = \yii\helpers\Url::to(['/user/view', 'id' => $model->getAttribute('id')]);
                    }
                    if ($action === 'edit') {
                        $url = \yii\helpers\Url::to(['/user/update', 'id' => $model->getAttribute('id')]);
                    }
                    if ($action === 'delete') {
                        $url = \yii\helpers\Url::to(['/user/delete', 'id' => $model->getAttribute('id')]);
                    }
                    return $url;
                }
            ],
        ],
    ]); ?>


</div>
