<?php

namespace app\models;

class User extends \yii\db\ActiveRecord implements \yii\web\IdentityInterface
{
    public $id;
    public $username;
    public $password;
    public $authKey;
    public $accessToken;
    public $age;
    public $user_id;

    private static $users = [
        '100' => [
            'id' => '100',
            'username' => 'admin',
            'password' => 'admin',
            'authKey' => 'test100key',
            'accessToken' => '100-token',
        ],
        '101' => [
            'id' => '101',
            'username' => 'demo',
            'password' => 'demo',
            'authKey' => 'test101key',
            'accessToken' => '101-token',
        ],
    ];


    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        return isset(self::$users[$id]) ? new static(self::$users[$id]) : null;
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        foreach (self::$users as $user) {
            if ($user['accessToken'] === $token) {
                return new static($user);
            }
        }

        return null;
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        foreach (self::$users as $user) {
            if (strcasecmp($user['username'], $username) === 0) {
                return new static($user);
            }
        }

        return null;
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->authKey;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return $this->password === $password;
    }

    public function rules()
    {
        return [
            [['first_name', 'last_name', 'email', 'personal_code', 'phone', 'lang'], 'required'],
            [['active', 'dead'], 'boolean'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => \Yii::t('app', 'ID'),
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'email' => 'Email',
            'personal_code' => 'Personal Code',
            'phone' => 'Phone',
            'active' => 'Active',
            'dead' => 'Dead',
            'lang' => 'Lang'
        ];
    }

    public function fields()
    {
        return [
            'id' => \Yii::$app->user->id,
            'first_name',
            'last_name',
            'email',
            'personal_code',
            'phone',
            'age' => $this->getAgeFromPersonalCode($this->personal_code),
            'user_id'=> \Yii::$app->user->getId(),
            'lang'
        ];
    }

    public function afterFind()
    {
        $this->age = $this->getUserAge(null, $this->personal_code);
        parent::afterFind();
    }

    protected function getUserBDay($code)
    {
        $bDate = null;
        if ($code != null) {
            $year = substr($code, 1, 2);
            $month = substr($code, 3, 2);
            $day = substr($code, 5, 2);
            $bDate = new \Datetime($year . '-' . $month . '-' . $day);
        }
        return $bDate;
    }

    protected function getUserAge($matchDate, $code)
    {
        if (is_null($matchDate)) {
            $matchDate = new \Datetime();
        }
        return $matchDate->diff($this->getUserBDay($code))->y;
    }

    public function getAgeFromPersonalCode($code)
    {
        return $this->getUserAge(null, $code);
    }

    public function getAge()
    {
        return $this->age;
    }

    public function getUserId()
    {
        return $this->user_id;
    }
}
