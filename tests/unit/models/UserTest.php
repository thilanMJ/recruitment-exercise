<?php

namespace tests\models;

use app\models\User;

class UserTest extends \Codeception\Test\Unit
{
    public function testFindUserById()
    {
        expect_that($user = User::findIdentity(100));
        expect($user->username)->equals('admin');

        expect_not(User::findIdentity(999));
    }

    public function testFindUserByAccessToken()
    {
        expect_that($user = User::findIdentityByAccessToken('100-token'));
        expect($user->username)->equals('admin');

        expect_not(User::findIdentityByAccessToken('non-existing'));
    }

    public function testFindUserByUsername()
    {
        expect_that($user = User::findByUsername('admin'));
        expect_not(User::findByUsername('not-admin'));
    }

    /**
     * @depends testFindUserByUsername
     */
    public function testValidateUser($user)
    {
        $user = User::findByUsername('admin');
        expect_that($user->validateAuthKey('test100key'));
        expect_not($user->validateAuthKey('test102key'));

        expect_that($user->validatePassword('admin'));
        expect_not($user->validatePassword('123456'));
    }

    public function testValidateUserAge()
    {

        $user = new User();
        $user1_pc = "38707070003";
        $user1_age = "31";
        $user1_calculated = $user->getAgeFromPersonalCode($user1_pc);

        $this->assertTrue(($user1_age == $user1_calculated), ['Calculated value and given age are same.']);


        $user2_pc = "48909120002";
        $user2_age = "29";
        $user2_calculated = $user->getAgeFromPersonalCode($user2_pc);
        $this->assertTrue(($user2_age == $user2_calculated), ['Calculated value and given age are same.']);

        $user3_pc = "50308070004";
        $user3_age = "17";
        $user3_calculated = $user->getAgeFromPersonalCode($user3_pc);
        $this->assertTrue(($user3_age != $user3_calculated), ['Calculated value and given age are not same.']);


    }

}
